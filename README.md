# Package Registry


This is a project to hold issues specific to the Package:Package Registry group. More information about our group can be found in our handbook page here: https://about.gitlab.com/handbook/engineering/development/ops/package/package-registry.

Check out our docs here: https://docs.gitlab.com/ee/user/packages/package_registry if interested to learn more.
